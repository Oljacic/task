# Task Chuck Norris jokes

This is custom module for Drupal 8.
This module content is the one random joke for one and only invincible Chuck Norris.
Purpose of creating this module is primarily for learning and cracking Drupal 8 module development.

## Getting Started

Download module, downloaded folder copy to your site_name/modules/

### Installing

If you have drush install with this command:
    
    *drush en -y task*

If you don't have drush installed, you can install trough you Drupal site.
And steps are:

    1. Log in to your Drupal site.
    2. Go to manage>extend.
    3. You can search for the module typing 'taks' in search, or you can scroll and fin it.
    4. When you find it check module you want and at the bootom of the page press install.
    5. And if evreyting is ok, you should see drupal message that module is successfully installed.

## Running the tests

Go back to the home page and click to menu navigation link 'Chuck Norris Joke'. 
And there you go, one joke for the Chuck Norris randomly generated.
Cheers!

## Authors

* **Stefan** - *Back-end Developer* - [Profile](https://gitlab.com/Oljacic)
