<?php

namespace Drupal\task\Controller;


use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Controller\ControllerBase;
use GuzzleHttp\Exception\RequestException;
use Drupal\Core\Url;


class ChuckController extends ControllerBase {

    // Method that renders joke and link for the new joke
    public function render() {

        // Host and the path
        $host = \Drupal::request()->getHost();
        $path = \Drupal::service('path.current')->getPath();

        // Markup array
        $render = [
            '#markup' => '<p>'.$this->chuckPower().'</p>'
        ];

        // Link array
        $render['link'] = [
            '#title' => $this->t('New Joke'),
            '#type' => 'link',
            '#url' => Url::fromUri('http://'.$host.$path.'?new')
        ];

        return $render;

    }


    // Function for the grabbing joke.
    public function grabJoke() {

        $uri = "https://api.chucknorris.io/jokes/random";

        // Trying to grab joke, if success return the result of the joke.
        try {

            $response = \Drupal::httpClient()->get($uri);
            $data = $response->getBody();

            $jsonArray = json_decode($data,true);

            $key = "value";
            $joke = $jsonArray[$key];

            return $joke;

        }
        catch (RequestException $e) {

            return FALSE;

        }
    }


    // Function for setting cache.
    public function setCache() {

        $joke = $this->grabJoke();

        \Drupal::cache()->set(
                'joke',
                $joke,
                CacheBackendInterface::CACHE_PERMANENT
        );

        return $joke;
    }

    // Function is for checking if joke data is set to the cache.
    public function chuckCache() {

        $data_from_cache = \Drupal::cache()->get('joke');

        // If Statement checking if data in cache is null or empty, than set the grabbed data to the cache.
        // Else, just data exist in cache return it.
        if ( $data_from_cache == null or empty($data_from_cache)) {
            // Return method setCache
            return $this->setCache();

        } else {

            $cache = \Drupal::cache()->get('joke');

            return $cache->data;

        }

    }

    // Function for setting new joke to the cache.
    public function chuckPower() {

        if(isset($_GET['new'])) {
            return $this->setCache();
        } else {
            return $this->chuckCache();
        }


    }


}